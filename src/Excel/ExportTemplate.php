<?php

namespace Neneff\Export\Excel;


/**
 * @Deprecated
 * @package Neneff\Export\Excel
 */
class ExportTemplate extends AbstractExport
{
    protected $_headers = [];

    /**
     * ExportTemplate constructor.
     *
     * @param $header
     * @param string $filename
     *
     * @throws
     */
    public function __construct($header, $filename = 'template')
    {
        $this->_headers = $header;
        parent::__construct($filename);
    }

    /**
     * @inheritdoc
     */
    protected function _prepareExport()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    protected function _generateRowStyle($rowIndex, $row)
    {
        return null;
    }

    /**
     * @inheritdoc
     */
    protected function _generateCellStyle($colIndex, $rowIndex, $cell)
    {
        return null;
    }

    /**
     * Write $_headers in $_workbook active sheet
     * @return mixed
     */
    protected function _generateHeader()
    {
        return $this->_headers;
    }

}