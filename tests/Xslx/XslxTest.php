<?php

namespace Neneff\Export\Test\Xslx;


use Neneff\Export\Xslx\XslxExport;


class XslxTest extends XslxExport
{

    /**
     * @inheritdoc
     */
    protected function generateHeader()
    {
        return [
            ['column1', 'column2']
        ];
    }

    /**
     * @inheritdoc
     */
    protected function generateRows()
    {
        return [
            ['r1:c1', 'r1:c2']
        ];
    }

    /**
     * @inheritdoc
     */
    protected function processRow($row)
    {
        return $row;
    }

}
